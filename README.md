# EC2 Information Crawler
## Overview
This project is a small Django project to emulate user referrals for Users of an application. The authentication method 
used is Token authentication. Database used is SQLite3.

## Requirements
* Python (3.6.5)
* Django (2.0+)

## Quickstart
### Installation
* Setup python virtual environment (I use pyenv, virtualenv works too) and install requirements.
    ```bash
    $ pyenv activate venv
    $ (venv) pip install -r requirements.txt
    ```
* Setup the SQLite3 DB file
    ```bash
    $ (venv) python manage.py migrate
    ```

### Deployment
```bash
$ (venv) python manage.py runserver
```

### API
* `POST /user/auth/` - Create User endpoint
* `POST /user/auth/login/` - Login User endpoint (Response Token)
* `GET /user/referred/` - List of referred Users (Authorized Only)


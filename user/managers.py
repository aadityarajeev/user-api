from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password, **fields):
        user = self.model(
            email=self.normalize_email(email),
            **fields
        )
        user.set_password(password)
        user.save(using=self._db)

from django.conf.urls import url
from .views import CreateUserView, LoginUserView, ReferredListView

urlpatterns = [
    url(r'^auth/$', CreateUserView.as_view(), name='create-user'),
    url(r'^auth/login/$', LoginUserView.as_view(), name='login-user'),
    url(r'referred/', ReferredListView.as_view(), name='referred-user-list')
]

from rest_framework import serializers, models
from django.contrib.auth import get_user_model
from shortuuid import uuid

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        read_only_fields = ['code_id']
        fields = ['code_id', 'email', 'name', 'mobile_number', 'city', 'referral_code', 'password']
        extra_kwargs = {
            "referral_code": {
                "error_messages": {
                    "does_not_exist": "Invalid Referral Code"
                }
            },
            "password": {"write_only": True}
        }

    def create(self, validated_data):
        validated_password = validated_data.pop('password')
        user = UserModel.objects.create(**validated_data)
        user.code_id = uuid()
        user.set_password(validated_password)
        user.save()
        return user


class ReferralUserSerializer(serializers.ModelSerializer):
    registered_at = serializers.DateTimeField(source='created')

    class Meta:
        model = UserModel
        fields = ['name', 'registered_at']

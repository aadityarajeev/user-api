from django.utils import timezone
from django.contrib.auth.base_user import AbstractBaseUser
from django.core import validators
from django.db import models

from .managers import UserManager


class User(AbstractBaseUser):
    phone_validator = validators.RegexValidator(regex=r'^\+\d{8,15}$',
                                                message="Phone Numbers are accepted in the E.164 standardized format. "
                                                        "Up to 15 digits are allowed")
    code_id = models.CharField(max_length=25, editable=False, db_index=True, unique=True)
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=60, blank=False)
    mobile_number = models.CharField(validators=[phone_validator], max_length=16, blank=False)
    city = models.CharField(max_length=20, blank=False)
    referral_code = models.ForeignKey('self', to_field='code_id', db_column='referral_code',
                                      blank=True, null=True, default=None, on_delete=models.SET_NULL)
    created = models.DateTimeField(default=timezone.now, editable=False)
    modified = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'mobile_number', 'city']

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        return super(User, self).save(*args, **kwargs)
